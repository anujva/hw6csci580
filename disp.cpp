/*   CS580 HW   */
#include    "stdafx.h"  
#include	"Gz.h"
#include	"disp.h"

int GzNewFrameBuffer(char** framebuffer, int width, int height)
{
        if( !framebuffer )
                return GZ_FAILURE;

        char *framebuffer_ph = new char[width*height*3];
        
		*framebuffer = framebuffer_ph;

        if(!(*framebuffer))
            return GZ_FAILURE;
		else if(!framebuffer)
			return GZ_FAILURE;
        return GZ_SUCCESS;
}

int GzNewDisplay(GzDisplay **display, GzDisplayClass dispClass, int xRes, int yRes)
{
        if( !display )
                return GZ_FAILURE;

        if( xRes < 0 || yRes < 0 || xRes > MAXXRES || yRes > MAXYRES )
                return GZ_FAILURE;

        GzDisplay * display_ph = new GzDisplay;

        if( !display_ph )
                return GZ_FAILURE;
		
        display_ph->fbuf = new GzPixel[xRes * yRes];
        display_ph->open = 1;
        display_ph->xres = xRes;
        display_ph->yres = yRes;
        display_ph->dispClass = GZ_RGBAZ_DISPLAY;

        if( !( display_ph->fbuf ) ){
            delete display_ph;
            return GZ_FAILURE;
        }
        
        *display = display_ph;
        return GZ_SUCCESS;
}


int GzFreeDisplay(GzDisplay     *display)
{
        if( !display )
                return GZ_FAILURE;

		delete display;
        return GZ_SUCCESS;
}


int GzGetDisplayParams(GzDisplay *display, int *xRes, int *yRes, GzDisplayClass *dispClass)
{
        if( !display || !xRes || !yRes || !dispClass )
                return GZ_FAILURE;

        *xRes = display->xres;
        *yRes = display->yres;
        *dispClass = display->dispClass;

        return GZ_SUCCESS;
}


int GzInitDisplay(GzDisplay     *display)
{
        if( !display )
                return GZ_FAILURE;

        for( int i = 0; i < display->xres; i++ ){
                for( int j = 0; j < display->yres; j++ ){
                        display->fbuf[ARRAY( i, j )].red = 233 << 4;
                        display->fbuf[ARRAY( i, j )].green = 10 << 4;
                        display->fbuf[ARRAY( i, j )].blue = 10 << 4;
                        display->fbuf[ARRAY( i, j )].alpha = 255;
                        display->fbuf[ARRAY( i, j )].z = INT_MAX;
                }
        }

        return GZ_SUCCESS;
}


int GzPutDisplay(GzDisplay *display, int i, int j, GzIntensity r, GzIntensity g, GzIntensity b, GzIntensity a, GzDepth z)
{
		if( !display )
                return GZ_FAILURE;

        if( i < 0 || j < 0 || i >= display->xres || j >= display->yres )
                return GZ_FAILURE;
		
		//clamp
        if(r<0)
            r = 0;
        else if(r>4095)
			r = 4095;

        if(g<0)
            g = 0;
        else if(g>4095)
            g = 4095;

        if(b<0)
            b = 0;
        else if(b>4095)
            b = 4095;

        display->fbuf[ARRAY( i, j )].red = r;
        display->fbuf[ARRAY( i, j )].green = g;
        display->fbuf[ARRAY( i, j )].blue = b;
        display->fbuf[ARRAY( i, j )].alpha = a;
        display->fbuf[ARRAY( i, j )].z = z;

        return GZ_SUCCESS;
}


int GzGetDisplay(GzDisplay *display, int i, int j, GzIntensity *r, GzIntensity *g, GzIntensity *b, GzIntensity *a, GzDepth *z)
{
        if( !display || !r || !g || !b || !a || !z )
                return GZ_FAILURE;

        if( i < 0 || j < 0 || i > display->xres - 1 || j > display->yres - 1)
                return GZ_FAILURE;

        *r = display->fbuf[ARRAY( i, j )].red;
        *g = display->fbuf[ARRAY( i, j )].green;
        *b = display->fbuf[ARRAY( i, j )].blue;
        *a = display->fbuf[ARRAY( i, j )].alpha;
        *z = display->fbuf[ARRAY( i, j )].z;

        return GZ_SUCCESS;
}


int GzFlushDisplay2File(FILE* outfile, GzDisplay *display)
{
        if( !outfile || !display )
                return GZ_FAILURE;

        char *buf;
		buf = new char[1024];
        printf(buf, 1024, "P6 %d %d 255\r", display->xres, display->yres);
        fwrite(buf, 1, strlen(buf), outfile);

        for( int i = 0; i < display->yres; i++ ){
            for( int j = 0; j < display->xres; j++ ){
				buf = new char[1024];
				printf( buf, 1024, "%c%c%c", display->fbuf[ARRAY(j, i)].red >> 4, display->fbuf[ARRAY(j, i)].green >> 4, display->fbuf[ARRAY(j, i)].blue >> 4 );
                fwrite(buf, 1, strlen(buf), outfile);
            }
        }

        return GZ_SUCCESS;
}

int GzFlushDisplay2FrameBuffer(char* framebuffer, GzDisplay *display)
{
        if( !framebuffer || !display )
                return GZ_FAILURE;

        for( int row = 0; row < display->xres; row++ ){
            for( int col = 0; col < display->yres; col++ ){
                char red = display->fbuf[ARRAY( col, row )].red >> 4;
                char green = display->fbuf[ARRAY( col, row )].green >> 4;
                char blue = display->fbuf[ARRAY( col, row )].blue >> 4;

                framebuffer[ARRAY( col, row )*3] = blue;
                framebuffer[ARRAY( col, row )*3+1] = green;
                framebuffer[ARRAY( col, row )*3+2] = red;
            }
        }

        return GZ_SUCCESS;
}