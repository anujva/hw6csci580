/* Texture functions for cs580 GzLib	*/
#include    "stdafx.h" 
#include	"stdio.h"
#include "math.h"
#include	"Gz.h"


GzColor	*image;
int xs, ys;
int reset = 1;

int index(int x, int y, int res){
	return x+y*res;
}

/* Image texture function */
int tex_fun(float u, float v, GzColor color)
{
  unsigned char		pixel[3];
  unsigned char     dummy;
  char  		foo[8];
  int   		i, j;
  FILE			*fd;

  if (reset) {          /* open and load texture file */
    fd = fopen ("texture", "rb");
    if (fd == NULL) {
      fprintf (stderr, "texture file not found\n");
      exit(-1);
    }
    fscanf (fd, "%s %d %d %c", foo, &xs, &ys, &dummy);
    image = (GzColor*)malloc(sizeof(GzColor)*(xs+1)*(ys+1));
    if (image == NULL) {
      fprintf (stderr, "malloc for texture image failed\n");
      exit(-1);
    }

    for (i = 0; i < xs*ys; i++) {	/* create array of GzColor values */
      fread(pixel, sizeof(pixel), 1, fd);
      image[i][RED] = (float)((int)pixel[RED]) * (1.0 / 255.0);
      image[i][GREEN] = (float)((int)pixel[GREEN]) * (1.0 / 255.0);
      image[i][BLUE] = (float)((int)pixel[BLUE]) * (1.0 / 255.0);
      }

    reset = 0;          /* init is done */
	fclose(fd);
  }

/* bounds-test u,v to make sure nothing will overflow image array bounds */
/* determine texture cell corner values and perform bilinear interpolation */
/* set color to interpolated GzColor value and return */
  if(u<0) u=0;
  if(u>1) u=1;
  if(v<0) v=0;
  if(v>1) v=1;

  float texX = u*(xs - 1);
  float texY = v*(ys - 1);

  float xmin = floor(texX);
  float ymin = floor(texY);
  float xmax = ceil(texX);
  float ymax = ceil(texY);

  GzColor col1, col2, col3, col4;
  memcpy(col1, image[ index(xmin, ymin, xs)], sizeof(GzColor));
  memcpy(col2, image[ index(xmax, ymin, xs)], sizeof(GzColor));
  memcpy(col3, image[ index(xmin, ymax, xs)], sizeof(GzColor));
  memcpy(col4, image[ index(xmax, ymax, xs)], sizeof(GzColor));

  GzColor cTrow, cBrow;
  float left = xmax - texX;
  float top = ymax - texY;

  for(int i=0; i<3; i++){
	  cTrow[i] = (left*col1[i])+((1-left)*col2[i]);
	  cBrow[i] = (left*col3[i])+((1-left)*col4[i]);
	  color[i] = (top*cTrow[i])+((1-top)*cBrow[i]);
  }
  return 0;
}

class CNumber
{
public:
	float real; 
	float imaginary;
};

/* Procedural texture function */
/* The Julia set code implemented can be found at the following url : http://lodev.org/cgtutor/juliamandelbrot.html
 * An example image of the Julia Set can be found at this link : http://lodev.org/cgtutor/images/juliaset.gif
 */

int ptex_fun(float u, float v, GzColor color)
{
	int N = 500;
	CNumber x;
	CNumber c;
	c.real = -0.7;
	c.imaginary= 0.27015;
	x.real = 2*u-0.7;
	x.imaginary = v-0.9;
	float newcolor;
	int count = 0;
	for(int i = 0; i < N; i++) {
		float xreal = x.real * x.real - x.imaginary * x.imaginary + c.real;
		float ximaginary = 2*x.real*x.imaginary + c.imaginary;
		if ((x.real * x.real + x.imaginary*x.imaginary) > 4.0)
			break;
		x.real = xreal;
		x.imaginary = ximaginary;
		count++;
	}

	float len = sqrt(x.real * x.real+ x.imaginary * x.imaginary);
	if (count == N){
		color[0] = len/2;
		color[1] = len/2;
		color[2] = len/2;
	}else{
		newcolor = ((float)count) / N;
		color[0] = newcolor * 10;
		color[1] = newcolor * 8;
		color[2] = newcolor * 10;
	}
	return GZ_SUCCESS;
}

