The entire project has been zipped and sent across this time in my submission. The Visual Studio Version that I have used to work on this project is 2010 version.

1. Gouraud Shading requires the interpStyle to be changed to GZ_COLOR in Application5.cpp
2. Phong Shading requires the interpStyle to be GZ_NORMAL in Application5.cpp
3. The texture implemented in the procedural texture function is the Julia Set. The values used for the Set has been referenced from the following website: 
	http://lodev.org/cgtutor/juliamandelbrot.html 
	
There has been no change in the API so everything should work with any skeletal HW5 files. Thanks.