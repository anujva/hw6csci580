﻿/* CS580 Homework 5 */

#include	"stdafx.h"
#include	"stdio.h"
#include	"math.h"
#include	"Gz.h"
#include	"rend.h"
#include <cstdlib>

#define COLORED_EDGE 0
#define NOCOLOR_EDGE 1
#define PI 3.1415926535897

class GzCoordClass{
public:
    GzCoord vertex;
    int originalIndex;
};

class Edge{
public:
	GzCoordClass end1;
	GzCoordClass end2;
	int edgeColor;

	static void copyEdge(GzCoord &vertex1, GzCoord &vertex2){
		for(int i=0; i<3; i++){
			vertex1[i] = vertex2[i];
		}
	}

	static void copyEdge(GzCoordClass &vertex1, GzCoordClass &vertex2){
		for(int i=0; i<3; i++){
			vertex1.vertex[i] = vertex2.vertex[i];
		}
		vertex1.originalIndex = vertex2.originalIndex;
	}
};

void vectorProd(GzCoord vec1, GzCoord vec2, GzCoord result){
	 result[X] = vec1[Y] * vec2[Z] - vec1[Z] * vec2[Y];
     result[Y] = vec1[Z] * vec2[X] - vec1[X] * vec2[Z];
     result[Z] = vec1[X] * vec2[Y] - vec1[Y] * vec2[X];
}

float dotProduct(GzCoord vec1, GzCoord vec2 ){
    return vec1[X] * vec2[X] + vec1[Y] * vec2[Y] + vec1[Z] * vec2[Z];
}

int compareX(const void* vertex1, const void *vertex2){
	float diff = ((GzCoordClass *)vertex1)->vertex[X] - ((GzCoordClass *)vertex2)->vertex[X];
	if(diff > 0){
		return 1;
	}else if(diff < 0){
		return -1;
	}else{
		return 0;
	}
}

int compareYX(const void* vertex1, const void *vertex2){
	float diff = ((GzCoordClass *)vertex1)->vertex[Y] - ((GzCoordClass *)vertex2)->vertex[Y];
	if(diff > 0){
		return 1;
	}else if(diff < 0){
		return -1;
	}else{
		float diff1 = ((GzCoordClass *)vertex1)->vertex[X] - ((GzCoordClass *)vertex2)->vertex[X];
		if(diff1>0){
			return 1;
		}else if (diff1<0){
			return -1;
		}else{
			return 0;
		}
	}
}

bool vectorSub( const GzCoord vec1, const GzCoord vec2, GzCoord difference )
{
        difference[X] = vec1[X] - vec2[X];
        difference[Y] = vec1[Y] - vec2[Y];
        difference[Z] = vec1[Z] - vec2[Z];
        return true;
}

bool scaleVec( const GzCoord vector, float scaleFactor, GzCoord result )
{
        result[X] = vector[X] * scaleFactor;
        result[Y] = vector[Y] * scaleFactor;
        result[Z] = vector[Z] * scaleFactor;
        return true;
}

class LeeRastizer{
private:
	static short	 ctoi(float color)		/* convert float color to GzIntensity short */
	{
	  return(short)((int)(color * ((1 << 12) - 1)));
	}

	static bool sortVertexCounter( Edge edges[3], GzCoord * vertex ){
		if( !vertex ){
			return false;
		}
		//@TODO
		GzCoordClass Vertices[3];

        for( int vertIdx = 0; vertIdx < 3; vertIdx++ )
        {
                memcpy( Vertices[vertIdx].vertex, vertex[vertIdx], sizeof( GzCoord ) );
                Vertices[vertIdx].originalIndex = vertIdx;
        }

		std::qsort( Vertices, 3, sizeof( GzCoordClass ), compareYX);

		if( Vertices[0].vertex[Y] == Vertices[1].vertex[Y] ){
			Edge::copyEdge(edges[0].end1, Vertices[1]);
			Edge::copyEdge(edges[0].end2, Vertices[0]);
			edges[0].edgeColor = COLORED_EDGE;
			Edge::copyEdge(edges[1].end1, Vertices[0]);
			Edge::copyEdge(edges[1].end2, Vertices[2]);
			edges[1].edgeColor = COLORED_EDGE;
			Edge::copyEdge(edges[2].end1, Vertices[2]);
			Edge::copyEdge(edges[2].end2, Vertices[1]);
			edges[2].edgeColor = NOCOLOR_EDGE;
		}
		else if( Vertices[1].vertex[Y] == Vertices[2].vertex[Y] ){
			Edge::copyEdge(edges[0].end1, Vertices[0]);
			Edge::copyEdge(edges[0].end2, Vertices[1]);
			edges[0].edgeColor = COLORED_EDGE;
			Edge::copyEdge(edges[1].end1, Vertices[1]);
			Edge::copyEdge(edges[1].end2, Vertices[2]);
			edges[1].edgeColor = NOCOLOR_EDGE;
			Edge::copyEdge(edges[2].end1, Vertices[2]);
			Edge::copyEdge(edges[2].end2, Vertices[0]);
			edges[2].edgeColor = NOCOLOR_EDGE;
		}
		else{
			float slope = (Vertices[2].vertex[Y] - Vertices[0].vertex[Y])/(Vertices[2].vertex[X] - Vertices[0].vertex[X]);
			float midpointX = ((Vertices[1].vertex[Y] - Vertices[0].vertex[Y])/slope) + Vertices[0].vertex[X];

			if( midpointX < Vertices[1].vertex[X] ){
				Edge::copyEdge(edges[0].end1, Vertices[0]);
				Edge::copyEdge(edges[0].end2, Vertices[2]);
				edges[0].edgeColor = COLORED_EDGE;

				Edge::copyEdge(edges[1].end1, Vertices[2]);
				Edge::copyEdge(edges[1].end2, Vertices[1]);
				edges[1].edgeColor = NOCOLOR_EDGE;

				Edge::copyEdge(edges[2].end1, Vertices[1]);
				Edge::copyEdge(edges[2].end2, Vertices[0]);
				edges[2].edgeColor = NOCOLOR_EDGE;
			}else if( midpointX > Vertices[1].vertex[X] ){
				Edge::copyEdge(edges[0].end1, Vertices[0]);
				Edge::copyEdge(edges[0].end2, Vertices[1]);
				edges[0].edgeColor = COLORED_EDGE;
				Edge::copyEdge(edges[1].end1, Vertices[1]);
				Edge::copyEdge(edges[1].end2, Vertices[2]);
				edges[1].edgeColor = COLORED_EDGE;
				Edge::copyEdge(edges[2].end1, Vertices[2]);
				Edge::copyEdge(edges[2].end2, Vertices[0]);
				edges[2].edgeColor = NOCOLOR_EDGE;
			}else{
				//error
				return false;
			}
		}
		return true;
	}

	static bool getBB(float *xmin, float *xmax, float *ymin, float *ymax, GzCoord *vertex){
		if(!xmin || !xmax || !ymin || !ymax || !vertex){
			return false;
		}

		//@TODO
		GzCoordClass Vertices[3];

        for( int i = 0; i < 3; i++ ){
            memcpy( Vertices[i].vertex, vertex[i], sizeof( GzCoord ) );
            Vertices[i].originalIndex = i;
        }
		qsort(Vertices, 3, sizeof(GzCoordClass), compareX);
		*xmin = Vertices[0].vertex[X];
		*xmax = Vertices[2].vertex[X];

		qsort(Vertices, 3, sizeof(GzCoordClass), compareYX);
		*ymin = Vertices[0].vertex[Y];
		*ymax = Vertices[2].vertex[Y];

		return true;
	}

	static bool getABC(float *A, float *B, float *C, Edge edge){
		if(!A||!B||!C){
			return false;
		}

		*A = edge.end2.vertex[1] - edge.end1.vertex[1];
		*B = -1*(edge.end2.vertex[0] - edge.end1.vertex[0]);
		*C = ( (edge.end2.vertex[0] - edge.end1.vertex[0]) * edge.end1.vertex[1] ) - ( (edge.end2.vertex[1] - edge.end1.vertex[1]) * edge.end1.vertex[0] );

		return true;
	}

	static bool vectorProduct(GzCoord result, Edge edge1, Edge edge2){
		 if( !result ){
			return false;
        }

        GzCoord vector1, vector2;

		vector1[0] = edge1.end2.vertex[0] - edge1.end1.vertex[0];
		vector1[1] = edge1.end2.vertex[1] - edge1.end1.vertex[1];
		vector1[2] = edge1.end2.vertex[2] - edge1.end1.vertex[2];
		vector2[0] = edge2.end2.vertex[0] - edge2.end1.vertex[0];
		vector2[1] = edge2.end2.vertex[1] - edge2.end1.vertex[1];
		vector2[2] = edge2.end2.vertex[2] - edge2.end1.vertex[2];
		result[X] = vector1[Y] * vector2[Z] - vector1[Z] * vector2[Y];
		result[Y] = -( vector1[X] * vector2[Z] - vector1[Z] * vector2[X] );
		result[Z] = vector1[X] * vector2[Y] - vector1[Y] * vector2[X];

        return true;
	}

public:
	static bool norm( GzCoord vector ){
        float mag = sqrt( vector[X] * vector[X] + vector[Y] * vector[Y] + vector[Z] * vector[Z] );
        vector[X] /= mag;
        vector[Y] /= mag;
        vector[Z] /= mag;
        return true;
	}

	static bool compute( GzRender * render, GzCoord iSVert, GzCoord iSNormal, GzTextureIndex texCoord, GzColor color )
	{
			if( !render )
					return false;
			color[0] = color[1] = color[2] = 0;
			GzColor Ks, Kd;
			Ks[0] = Ks[1] = Ks[2] = 0;
			Kd[0] = Kd[1] = Kd[2] = 0;
			for( int i = 0; i < render->numlights; i++ ){
				float N_L = dotProduct( iSNormal, render->lights[i].direction );
				GzCoord iDir;
				iDir[X] = 0; 
				iDir[Y] = 0;
				iDir[Z] = -1;
				float N_E = dotProduct( iSNormal, iDir );
				GzCoord newISNormal;
				if( N_L > 0 && N_E > 0 ){
					newISNormal[X] = iSNormal[X];
					newISNormal[Y] = iSNormal[Y];
					newISNormal[Z] = iSNormal[Z];
				}else if( N_L < 0 && N_E < 0 ){
					scaleVec( iSNormal, -1, newISNormal );
					N_L *= -1;
					N_E *= -1;
				}else{
					continue;
				}   
				GzCoord NL_N, refRay;
				scaleVec(newISNormal, 2 * N_L, NL_N);
				vectorSub(NL_N, render->lights[i].direction, refRay);
				norm(refRay);
				float R_E = dotProduct(refRay, iDir);
				if( R_E < 0 )
						R_E = 0;
				GzColor tempKs, tempKd;
				scaleVec(render->lights[i].color, pow(R_E, render->spec), tempKs);
				scaleVec(render->lights[i].color, N_L, tempKd);       

				addVec(Ks, tempKs, Ks);
				addVec(Kd, tempKd, Kd);
			}

			bool GouraudTexture = false;
			GzColor Ka_ph, Kd_ph, Ks_ph;

			if(render->tex_fun){
				if(render->interp_mode == GZ_COLOR){
					GouraudTexture = true;
				}
				else{
					GzColor tColor;
					render->tex_fun(texCoord[U], texCoord[V], tColor);
					memcpy(Ka_ph, tColor, sizeof(GzColor));
					memcpy(Kd_ph, tColor, sizeof(GzColor));
					memcpy(Ks_ph, tColor, sizeof(GzColor));
				}
			}else{
				memcpy(Ka_ph, render->Ka, sizeof(GzColor));
				memcpy(Kd_ph, render->Kd, sizeof(GzColor));
				memcpy(Ks_ph, render->Ks, sizeof(GzColor));
			}

			if(GouraudTexture == true){
				addVec(Ks, Kd, color);
				addVec(render->ambientlight.color, color, color);
			}else{
				GzCoord Ka;
				vecComMul(Ks_ph, Ks, Ks);
				vecComMul(Kd_ph, Kd, Kd);
				vecComMul(Ka_ph, render->ambientlight.color, Ka);
				addVec(Ks, Kd, color);
				addVec(Ka, color, color);
			}
			
			return true;
	}

	static bool addVec( const GzCoord vec1, const GzCoord vec2, GzCoord result ){
		result[X] = vec1[X] + vec2[X];
		result[Y] = vec1[Y] + vec2[Y];
		result[Z] = vec1[Z] + vec2[Z];
		return true;
	}

	static bool vecComMul( const GzCoord vec1, const GzCoord vec2, GzCoord prod ){
		for( int idx = 0; idx < 3; idx++ ){
				prod[idx] = vec1[idx] * vec2[idx];
		}
		return true;
	}

	static float planeVal(float pA, float pB, float pC, GzCoord *sSVertex){
		return -(pA*sSVertex[0][X] + pB*sSVertex[0][Y] + pC*sSVertex[0][Z]);
	}

	static float planeValF(float pA, float pB, float pC, float x, float y, float p){
		return -(pA*x + pB*y + pC*p);
	}

	static float interpolatePC(float pA, float pB, float pC, float pD, float x, float y){
		return -(pA*x + pB*y +pD)/pC;
	}

	static float cVZ(float x){
		return x/(INT_MAX-x);
	}

	static float iSPtoP(float *a, GzTextureIndex* b, int i, int j){
		float x= a[i];
		float y= b[i][j];
		return y/(cVZ(x)+1);
	}

	static float pSPtoISP(float x, float y){
		return y*(cVZ(x)+1);
	}

	LeeRastizer(void);
	~LeeRastizer(void);

	static bool rasterize(GzRender *render, GzCoord* sSVertex, GzCoord *iSVertex, GzCoord *iSNormals, GzTextureIndex *texCoord){
		float xMinimum, xMaximum, yMinimum, yMaximum;
		if( !getBB( &xMinimum, &xMaximum, &yMinimum, &yMaximum, sSVertex))
                return false;

        Edge edges[3];
		if(!sortVertexCounter(edges, sSVertex))
                return false; 

		float edge00, edge01, edge02, edge10, edge11, edge12, edge20, edge21, edge22;
        getABC(&edge00, &edge01, &edge02, edges[0]);
        getABC(&edge10, &edge11, &edge12, edges[1]);
        getABC(&edge20, &edge21, &edge22, edges[2]);

		GzCoord crossVector;
        if( !( vectorProduct( crossVector, edges[0], edges[1] ) ) )
                return false;

        float pA = crossVector[X];
        float pB = crossVector[Y];
        float pC = crossVector[Z];
		float pD = planeValF(pA, pB, pC, sSVertex[0][X], sSVertex[0][Y], sSVertex[0][Z]);//-( pA * sSVertex[0][X] + pB * sSVertex[0][Y] + pC * sSVertex[0][Z] );

		GzColor cPA, cPB, cPC, cPD;
        GzCoord iSVertsPA, iSVertsPB, iSVertsPC, iSVertsPD;
        GzCoord iSNormalsPA, iSNormalsPB, iSNormalsPC, iSNormalsPD;
        GzCoord iHelper1, iHelper2, interpCP;
        iHelper1[X] = edges[0].end2.vertex[X] - edges[0].end1.vertex[X];
        iHelper1[Y] = edges[0].end2.vertex[Y] - edges[0].end1.vertex[Y];
        iHelper2[X] = edges[1].end2.vertex[X] - edges[1].end1.vertex[X];
        iHelper2[Y] = edges[1].end2.vertex[Y] - edges[1].end1.vertex[Y];

		GzTextureIndex texPlaneA, texPlaneB, texPlaneC, texPlaneD, perTexCoord[3];
		float screenZ[3];
		for(int i=0; i<3; i++){
			screenZ[i] = interpolatePC(pA, pB, pC, pD, sSVertex[i][0], sSVertex[i][1]);
			perTexCoord[i][U] = iSPtoP(screenZ, texCoord, i, U);
			perTexCoord[i][V] = iSPtoP(screenZ, texCoord, i, V);
		}

		for(int i=0; i<2; i++){
			iHelper1[2] = perTexCoord[edges[0].end2.originalIndex][i] - perTexCoord[edges[0].end1.originalIndex][i];
			iHelper2[2] = perTexCoord[edges[1].end2.originalIndex][i] - perTexCoord[edges[1].end1.originalIndex][i];
			vectorProd(iHelper1, iHelper2, interpCP);
			texPlaneA[i] = interpCP[0];
			texPlaneB[i] = interpCP[1];
			texPlaneC[i] = interpCP[2];
			texPlaneD[i] = planeValF(texPlaneA[i], texPlaneB[i], texPlaneC[i], edges[0].end1.vertex[X], edges[0].end1.vertex[Y], perTexCoord[edges[0].end1.originalIndex][i]);
		}

		if(render->interp_mode == GZ_COLOR){
			GzColor vColor[3];
			for(int i=0; i<3; i++){
				compute(render, iSVertex[i], iSNormals[i], texCoord[i], vColor[i]);
			}

			for(int i=0; i<3; i++){
				iHelper1[Z] = vColor[edges[0].end2.originalIndex][i] - vColor[edges[0].end1.originalIndex][i];
				iHelper2[Z] = vColor[edges[1].end2.originalIndex][i] - vColor[edges[1].end1.originalIndex][i];
				vectorProd(iHelper1, iHelper2, interpCP);
				cPA[i] = interpCP[0];
				cPB[i] = interpCP[1];
				cPC[i] = interpCP[2];
				cPD[i] = planeValF(cPA[i], cPB[i], cPC[i], edges[0].end1.vertex[0], edges[0].end1.vertex[1], vColor[edges[0].end1.originalIndex][i]);
			}
		}

		if(render->interp_mode == GZ_NORMALS){
			for(int i=0; i<3; i++){
				iHelper1[Z] = iSVertex[edges[0].end2.originalIndex][i] - iSVertex[edges[0].end1.originalIndex][i];
				iHelper2[Z] = iSVertex[edges[1].end2.originalIndex][i] - iSVertex[edges[1].end1.originalIndex][i];
				vectorProd(iHelper1, iHelper2, interpCP);
				iSVertsPA[i] = interpCP[0];
				iSVertsPB[i] = interpCP[1];
				iSVertsPC[i] = interpCP[2];
				iSVertsPD[i] = planeValF(iSVertsPA[i], iSVertsPB[i], iSVertsPC[i], edges[0].end1.vertex[0], edges[0].end1.vertex[1], iSVertex[edges[0].end1.originalIndex][i]);

				iHelper1[Z] = iSNormals[edges[0].end2.originalIndex][i] - iSNormals[edges[0].end1.originalIndex][i];
				iHelper2[Z] = iSNormals[edges[1].end2.originalIndex][i] - iSNormals[edges[1].end1.originalIndex][i];
				vectorProd(iHelper1, iHelper2, interpCP);
				iSNormalsPA[i] = interpCP[0];
				iSNormalsPB[i] = interpCP[1];
				iSNormalsPC[i] = interpCP[2];
				iSNormalsPD[i] = planeValF(iSNormalsPA[i], iSNormalsPB[i], iSNormalsPC[i], edges[0].end1.vertex[0], edges[0].end1.vertex[1], iSNormals[edges[0].end1.originalIndex][i]);
			}
		}

		int xBegin = max( (int)(ceil( xMinimum)),0);
        int yBegin = max( (int)( ceil( yMinimum ) ), 0 );
        int xEnd = min( (int)( floor( xMaximum ) ), render->display->xres - 1 );
        int yEnd = min( (int)( floor( yMaximum ) ), render->display->yres - 1 );

		for(int i = yBegin; i <= yEnd; i++){
            for(int j = xBegin; j <= xEnd; j++){
                bool isSHADED = false;

                float firstResult = edge00 * j + edge01 * i + edge02;
                if( firstResult == 0 ){
					if( edges[0].edgeColor == COLORED_EDGE ){
						isSHADED = true;
					}else{
						continue;
					}
                }
                else if( firstResult < 0 )
                        continue;

                if( !isSHADED){
                    float secondResult = edge10 * j + edge11 * i + edge12;
                    if( secondResult == 0 ){
						if( edges[1].edgeColor == COLORED_EDGE )
                            isSHADED = true;
                        else
							continue; 
                    }
                    else if( secondResult < 0 )
                            continue;
                }

                if(!isSHADED){
                    float thirdResult = edge20 * j + edge21 * i + edge22;
                    if( thirdResult == 0 ){
                        if( edges[2].edgeColor == COLORED_EDGE )
                                isSHADED = true;
                        else
                                continue;
                    }
                    else if( thirdResult < 0 )
                            continue;
                }
                float Zind = interpolatePC(pA, pB, pC, pD, j, i);
                if( Zind < 0 )
                        continue;

                GzIntensity r, g, b, a;
                GzDepth z;
                if( GzGetDisplay( render->display, j, i, &r, &g, &b, &a, &z ) )
					continue;

                if(Zind < z ){
					GzColor newColor;
					if(render->interp_mode == GZ_COLOR){
						for(int l=0; l<3; l++){
							newColor[l] = interpolatePC(cPA[l], cPB[l], cPC[l], cPD[l], j, i);
						}
						if(render->tex_fun){
							GzTextureIndex interpTC;
							for(int l =0; l<2; l++){
								interpTC[l] = interpolatePC(texPlaneA[l], texPlaneB[l], texPlaneC[l], texPlaneD[l], j, i);
								interpTC[l] = pSPtoISP(Zind, interpTC[l]);
							}
							GzColor tColor;
							render->tex_fun(interpTC[U], interpTC[V], tColor);
							vecComMul(newColor, tColor, newColor);
						}
					}else if(render->interp_mode == GZ_NORMALS){
						GzCoord iISVertex, iISNormal;
						GzTextureIndex interpTC;
						for(int l=0; l<3; l++){
							iISVertex[l] = interpolatePC(iSVertsPA[l], iSVertsPB[l], iSVertsPC[l], iSVertsPD[l], j, i);
							iISNormal[l] = interpolatePC(iSNormalsPA[l], iSNormalsPB[l], iSNormalsPC[l], iSNormalsPD[l], j, i);
						}
						for(int l=0; l<2; l++){
							interpTC[l] = interpolatePC(texPlaneA[l], texPlaneB[l], texPlaneC[l], texPlaneD[l], j, i);
							interpTC[l] = pSPtoISP(Zind, interpTC[l]);
						}
						norm(iISNormal);
						compute(render, iISVertex, iISNormal, interpTC, newColor);
					}else{
						newColor[RED] = render->flatcolor[RED];
						newColor[GREEN] = render->flatcolor[GREEN];
						newColor[BLUE] = render->flatcolor[BLUE];
					}

                    if(GzPutDisplay(render->display, j, i, ctoi(newColor[RED]), ctoi(newColor[GREEN]), ctoi(newColor[BLUE]), a, (GzDepth)Zind)){
                        return false;
                    }
                }
            } 
        }
		return GZ_SUCCESS;
	}
};

bool copyGzMatrix(GzMatrix mat1, GzMatrix mat2){
	if(!mat1 || !mat2){
		return false;
	}

	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			mat1[i][j] = mat2[i][j];
		}
	}

	return true;
}

int GzRotXMat(float degree, GzMatrix mat)
{
	if(!mat){
		return GZ_FAILURE;
	}
	
	GzMatrix mat_ph = {
		{1, 0, 0, 0},
		{0, (float)cos(degree*(PI/180)), -1*(float)sin(degree*(PI/180)), 0},
		{0, (float)sin(degree*(PI/180)), (float)cos(degree*(PI/180)), 0},
		{0, 0, 0, 1}
	};

	copyGzMatrix(mat, mat_ph);
	return GZ_SUCCESS;
}


int GzRotYMat(float degree, GzMatrix mat)
{
	if(!mat){
		return GZ_FAILURE;
	}
	
	GzMatrix mat_ph = {
		{(float)cos(degree*(PI/180)), 0, (float)sin(degree*(PI/180)), 0},
		{0, 1, 0, 0},
		{-1*(float)sin(degree*(PI/180)), 0, (float)cos(degree*(PI/180)), 0},
		{0, 0, 0, 1}
	};

	copyGzMatrix(mat, mat_ph);
	return GZ_SUCCESS;
}


int GzRotZMat(float degree, GzMatrix mat)
{
	if(!mat){
		return GZ_FAILURE;
	}
	
	GzMatrix mat_ph = {
		{(float)cos(degree*(PI/180)), -1*(float)sin(degree*(PI/180)), 0, 0},
		{(float)sin(degree*(PI/180)), (float)cos(degree*(PI/180)), 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1}
	};

	copyGzMatrix(mat, mat_ph);
	return GZ_SUCCESS;
}


int GzTrxMat(GzCoord translate, GzMatrix mat)
{
	if(!mat){
		return GZ_FAILURE;
	}
	
	GzMatrix mat_ph = {
		{1, 0, 0, translate[X]},
		{0, 1, 0, translate[Y]},
		{0, 0, 1, translate[Z]},
		{0, 0, 0, 1}
	};

	copyGzMatrix(mat, mat_ph);
	return GZ_SUCCESS;
}


int GzScaleMat(GzCoord scale, GzMatrix mat)
{
	if(!mat){
		return GZ_FAILURE;
	}
	
	GzMatrix mat_ph = {
		{scale[X], 0, 0, 0},
		{0, scale[Y], 0, 0},
		{0, 0, scale[Z], 0},
		{0, 0, 0, 1}
	};

	copyGzMatrix(mat, mat_ph);
	return GZ_SUCCESS;
}


//----------------------------------------------------------
// Begin main functions

int GzNewRender(GzRender **render, GzRenderClass renderClass, GzDisplay *display)
{
	if(!render || !display || (renderClass != GZ_Z_BUFFER_RENDER)){
		return GZ_FAILURE;
	}

	GzRender *render_ph = new GzRender;
	render_ph->display = display;
	render_ph->open = 0;
	render_ph->renderClass = renderClass;
	render_ph->camera.FOV = DEFAULT_FOV;
	render_ph->camera.lookat[X] = 0;
	render_ph->camera.lookat[Y] = 0;
	render_ph->camera.lookat[Z] = 0;
	render_ph->camera.position[X] = DEFAULT_IM_X;
	render_ph->camera.position[Y] = DEFAULT_IM_Y;
	render_ph->camera.position[Z] = DEFAULT_IM_Z;
	render_ph->camera.worldup[X] = 0;
	render_ph->camera.worldup[Y] = 1;
	render_ph->camera.worldup[Z] = 0;
	render_ph->numlights = 0;
	render_ph->interp_mode = GZ_RGB_COLOR;
	render_ph->spec = DEFAULT_SPEC;
	render_ph->ambientlight.color[RED] = 0.0f;
	render_ph->ambientlight.color[GREEN] = 0.0f;
	render_ph->ambientlight.color[BLUE] = 0.0f;
	GzColor Ka_default = DEFAULT_AMBIENT;
	GzColor Kd_default = DEFAULT_DIFFUSE;
	GzColor Ks_default = DEFAULT_SPECULAR;
	render_ph->Ka[RED] = Ka_default[0];
	render_ph->Ka[GREEN] = Ka_default[1];
	render_ph->Ka[BLUE] = Ka_default[2];
	render_ph->Kd[RED] = Kd_default[0];
	render_ph->Kd[GREEN] = Kd_default[1];
	render_ph->Kd[BLUE] = Kd_default[2];
	render_ph->Ks[RED] = Ks_default[0];
	render_ph->Ks[GREEN] = Ks_default[1];
	render_ph->Ks[BLUE] = Ks_default[2];
	render_ph->tex_fun = 0;

	//Homework 6
	render_ph->xshift = 0;
	render_ph->yshift = 0;

	*render = render_ph;
	return GZ_SUCCESS;
}


int GzFreeRender(GzRender *render)
{
/* 
-free all renderer resources
*/
	if(!render){
		return GZ_FAILURE;
	}

	delete render;
	return GZ_SUCCESS;
}

int GzBeginRender(GzRender	*render)
{
/* 
- set up for start of each frame - init frame buffer
*/
	if(!render)
		return GZ_FAILURE;

	render->flatcolor[RED] = 12;
	render->flatcolor[GREEN] = 1;
	render->flatcolor[BLUE] = 12;

	float d = 1/tan((float)(render->camera.FOV*(PI/180))/2);

	render->matlevel = -1;

	//Xpi
	float Xpi_ph[][4] = {
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 1/d, 1}
	};

	copyGzMatrix(render->camera.Xpi, Xpi_ph);

	//Xiw
	GzCoord cameraX, cameraY, cameraZ;
	
	//First Z
	cameraZ[X] = render->camera.lookat[X] - render->camera.position[X];
	cameraZ[Y] = render->camera.lookat[Y] - render->camera.position[Y];
	cameraZ[Z] = render->camera.lookat[Z] - render->camera.position[Z];

	float mag = sqrtf(cameraZ[X]*cameraZ[X] + cameraZ[Y]*cameraZ[Y] + cameraZ[Z]*cameraZ[Z]);
	cameraZ[X] = cameraZ[X] / mag;
	cameraZ[Y] = cameraZ[Y] / mag;
	cameraZ[Z] = cameraZ[Z] / mag;

	//Then Y
	float worldupDotCamZ = dotProduct(render->camera.worldup, cameraZ);
	GzCoord scaledCamZ;
	scaledCamZ[X] = cameraZ[X]*worldupDotCamZ;
	scaledCamZ[Y] = cameraZ[Y]*worldupDotCamZ;
	scaledCamZ[Z] = cameraZ[Z]*worldupDotCamZ;
	cameraY[X] = render->camera.worldup[X] - scaledCamZ[X];
	cameraY[Y] = render->camera.worldup[Y] - scaledCamZ[Y];
	cameraY[Z] = render->camera.worldup[Z] - scaledCamZ[Z];
	mag = sqrtf(cameraY[X]*cameraY[X] + cameraY[Y]*cameraY[Y] + cameraY[Z]*cameraY[Z]);
	cameraY[X] = cameraY[X]/mag;
	cameraY[Y] = cameraY[Y]/mag;
	cameraY[Z] = cameraY[Z]/mag;

	//Finally X
	vectorProd(cameraY, cameraZ, cameraX);
	GzMatrix Xiw_ph = {
		{cameraX[X], cameraX[Y], cameraX[Z], -1*dotProduct(cameraX, render->camera.position)},
		{cameraY[X], cameraY[Y], cameraY[Z], -1*dotProduct(cameraY, render->camera.position)},
		{cameraZ[X], cameraZ[Y], cameraZ[Z], -1*dotProduct(cameraZ, render->camera.position)},
		{0, 0, 0, 1}
	};

	copyGzMatrix(render->camera.Xiw, Xiw_ph);

	//Xsp
	GzMatrix Xsp_ph = {
		{render->display->xres/2.00, 0, 0, render->display->xres/2.00},
		{0, -1*render->display->yres/2.00, 0, render->display->yres/2.00},
		{0, 0, INT_MAX/d, 0},
		{0, 0, 0, 1}
	};

	copyGzMatrix(render->Xsp, Xsp_ph);

	render->matlevel = -1;

	int return_val = GZ_SUCCESS;
	return_val |= GzPushMatrix(render, render->Xsp);
	return_val |= GzPushMatrix(render, render->camera.Xpi);
	return_val |= GzPushMatrix(render, render->camera.Xiw);

	render->open = 1;
	return return_val;
}


int GzPutAttribute(GzRender	*render, int numAttributes, GzToken	*nameList, 
	GzPointer *valueList) /* void** valuelist */
{
	if(!render||!nameList||!valueList){
		return GZ_FAILURE;
	}

	for(int i=0; i<numAttributes; i++){
		if(nameList[i] == GZ_RGB_COLOR){
			render->flatcolor[RED] = (*(GzColor*)(valueList[i]))[RED];
			render->flatcolor[GREEN] = (*(GzColor*)(valueList[i]))[GREEN];
			render->flatcolor[BLUE] = (*(GzColor*)(valueList[i]))[BLUE];
		}
		if(nameList[i] == GZ_INTERPOLATE){
			render->interp_mode = *((int*)(valueList[i]));
		}
		if(nameList[i] == GZ_AMBIENT_LIGHT){
			memcpy(render->ambientlight.color, ((GzLight *)(valueList[i]))->color, sizeof(GzColor));
		}
		if(nameList[i] == GZ_AMBIENT_COEFFICIENT){
			GzColor *ambiCoeff = (GzColor *)valueList[i];
			render->Ka[0] = (*ambiCoeff)[0];
			render->Ka[	1] = (*ambiCoeff)[1];
			render->Ka[2] = (*ambiCoeff)[2];
		}
		if(nameList[i] == GZ_DIRECTIONAL_LIGHT){
			if(render->numlights < MAX_LIGHTS){
				memcpy(render->lights[render->numlights].color, ((GzLight *)valueList[i])->color, sizeof(GzColor));
				memcpy(render->lights[render->numlights].direction, ((GzLight *)valueList[i])->direction, sizeof(GzCoord));
				//@TODO
				LeeRastizer::norm( render->lights[render->numlights].direction );
				render->numlights++;
			}
		}
		if(nameList[i] == GZ_DIFFUSE_COEFFICIENT){
			GzColor *diffCoeff = (GzColor *)valueList[i];
			render->Kd[0] = (*diffCoeff)[0];
			render->Kd[	1] = (*diffCoeff)[1];
			render->Kd[2] = (*diffCoeff)[2];
		}
		if(nameList[i] == GZ_SPECULAR_COEFFICIENT){
			GzColor *specCoeff = (GzColor *)valueList[i];
			render->Ks[0] = (*specCoeff)[0];
			render->Ks[	1] = (*specCoeff)[1];
			render->Ks[2] = (*specCoeff)[2];
		}
		if(nameList[i] == GZ_DISTRIBUTION_COEFFICIENT){
			render->spec = *((float *)valueList[i]);
		}
		if(nameList[i] == GZ_TEXTURE_MAP){
			render->tex_fun = (GzTexture)(valueList[i]);
		}
		if(nameList[i] == GZ_AASHIFTX){
			float* pShiftx = (float *)valueList[i];
			render->xshift = *pShiftx;
		}
		if(nameList[i] = GZ_AASHIFTY){
			float* pShifty = (float *)valueList[i];
			render->yshift = *pShifty;
		}
	}

	return GZ_SUCCESS;
}

bool triThrowPlane(GzRender *render, GzCoord* vertex){
	if(!render || !vertex){
		return true;
	}

	if(!render->display){
		return true;
	}

	if(vertex[0][Y]<0 && vertex[1][Y]<0 && vertex[2][Y]<0){
		return true;
	}else if(vertex[0][X]<0 && vertex[1][X]<0 && vertex[2][X]<0){
		return true;
	}else if(vertex[0][X]> render->display->xres && vertex[1][X]>render->display->xres && vertex[2][X]>render->display->xres){
		return true;
	}else if(vertex[0][Y]>render->display->yres && vertex[1][Y]>render->display->yres && vertex[2][Y]>render->display->yres){
		return true;
	}

	return false;
}

void hMatMul(const GzMatrix mat, const GzCoord vec, GzCoord res){
	float tempRes[4];
    float tempVec[4];

    tempVec[X] = vec[X];
    tempVec[Y] = vec[Y];
    tempVec[Z] = vec[Z];
    tempVec[3] = 1;

    for( int i = 0; i < 4; i++ )
            tempRes[i] = 0;
        
    for( int i = 0; i < 4; i++ ){
        for( int j = 0; j < 4; j++ ){
            tempRes[i] += mat[i][j] * tempVec[j];
        }
    }
    res[X] = tempRes[X] / tempRes[3];
    res[Y] = tempRes[Y] / tempRes[3];
    res[Z] = tempRes[Z] / tempRes[3];
}

int GzPutTriangle(GzRender *render, int	numParts, GzToken *nameList,
	GzPointer *valueList) 
{
	if(!render || !nameList || !valueList){
		return GZ_FAILURE;
	}

	GzCoord * sSVerts = new GzCoord[3];
    GzCoord * iSVerts = new GzCoord[3];
    GzCoord * iSNormals = new GzCoord[3];
    GzCoord * mSVerts = 0;
    GzCoord * mSNormals = 0;
	GzTextureIndex *texCoord = 0;

	for(int i=0; i<numParts; i++){
		if(nameList[i] == GZ_NULL_TOKEN){

		}else if(nameList[i] == GZ_POSITION){
			mSVerts = (GzCoord*)valueList[i];            
            bool discard = false;
			for(int i=0; i<3; i++){
				hMatMul(render->Ximage[render->matlevel], mSVerts[i], sSVerts[i]); 
				if(sSVerts[i][Z]<0){
					discard = true;
					break;
				}
				sSVerts[i][0] -= render->xshift;
				sSVerts[i][1] -= render->yshift;
			}
			if(discard || triThrowPlane(render, sSVerts)){
				return GZ_SUCCESS;
			}
		}else if(nameList[i] == GZ_NORMAL){
			mSNormals = (GzCoord *)(valueList[i]);
		}else if(nameList[i] == GZ_TEXTURE_INDEX){
			texCoord = (GzTextureIndex *)valueList[i];
		}
	}

	if(!mSVerts || !mSNormals){
		return GZ_FAILURE;
	}

	for(int i=0; i<3; i++){
		hMatMul(render->Xnorm[render->matlevel], mSVerts[i], iSVerts[i]);
		hMatMul(render->Xnorm[render->matlevel], mSNormals[i], iSNormals[i]);
	}

	LeeRastizer::rasterize(render, sSVerts, iSVerts, iSNormals, texCoord);

	delete sSVerts;
	delete iSVerts;
	delete iSNormals;

	return GZ_SUCCESS;
}


int GzPutCamera(GzRender *render, GzCamera *camera)
{
	memcpy( &( render->camera ), camera, sizeof( GzCamera ) );

	return GZ_SUCCESS;	
}

void matMul(GzMatrix mat1, GzMatrix mat2, GzMatrix result){
	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			result[i][j] = 0;
		}
	}

	for(int i=0; i<4; i++){
		for(int j=0; j<4; j++){
			for(int k=0; k<4; k++){
				result[i][j] += mat1[i][k]*mat2[k][j];
			}
		}
	}
}

int GzPushMatrix(GzRender *render, GzMatrix	matrix)
{
	if( render->matlevel == -1 ){
		memcpy( render->Ximage[render->matlevel + 1], matrix, sizeof( GzMatrix ) );
	}else{
        GzMatrix xProduct;
        matMul( render->Ximage[render->matlevel], matrix, xProduct );
        memcpy( render->Ximage[render->matlevel + 1], xProduct, sizeof( GzMatrix ) );
    }
    render->matlevel++;

	if(render->matlevel == 0 || render->matlevel == 1){
		GzMatrix iden = {
			{1, 0, 0, 0},
			{0, 1, 0, 0},
			{0, 0, 1, 0},
			{0, 0, 0, 1}
		};
		memcpy(render->Xnorm[render->matlevel], iden, sizeof(GzMatrix));
	}else{
		GzMatrix matrix_ph;
		copyGzMatrix(matrix_ph, matrix);
		matrix_ph[0][3] = matrix_ph[1][3] = matrix_ph[2][3] = 0;
		matrix_ph[3][3] = 1;

		//normalize
		float normal = 1/sqrtf(matrix[0][0] * matrix[0][0] + matrix[1][0] * matrix[1][0] + matrix[2][0] * matrix[2][0]);
		for(int i=0; i<3; i++){
			for(int j=0; j<3; j++){
				matrix_ph[i][j] = matrix_ph[i][j] * normal;
			}
		}

		//multiply
		GzMatrix xProduct;
		matMul(render->Xnorm[render->matlevel - 1], matrix_ph, xProduct);
		memcpy(render->Xnorm[render->matlevel], xProduct, sizeof( GzMatrix ));
	}

	return GZ_SUCCESS;
}

int GzPopMatrix(GzRender *render)
{
	if(!render || render->matlevel == -1)
		return GZ_FAILURE;

	render->matlevel--;

	return GZ_SUCCESS;
}
